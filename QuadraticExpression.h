#pragma once
#include <string>

class QuadraticExpression
{
	int lengthExpression;
	char *quadraticExpression;
	int leadingCoefficient;
	int coefficientX;
	int freeCoefficient;
	int degree;

	void deleteSpaces();
	void analyzeString();
	int lengthStr(const char* const);
	int expressionToInt(const char*);
	bool stringToIntArray(const char*, int*, const int);
	void intToString(int);
public:
	QuadraticExpression();
	QuadraticExpression(char*);
	QuadraticExpression(std::string);
	QuadraticExpression(const QuadraticExpression &expression);

	friend std::ostream& operator<<(std::ostream& out, QuadraticExpression const expression);
	friend QuadraticExpression operator+(QuadraticExpression const expression_1, QuadraticExpression const expression_2);
	QuadraticExpression& operator=(QuadraticExpression& const expression);

	~QuadraticExpression();

};

