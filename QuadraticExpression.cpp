#pragma once
#include "QuadraticExpression.h"

void QuadraticExpression::deleteSpaces()
{
	int i = 0, length = 0;
	while (i < lengthExpression) {
		if (quadraticExpression[i++] == ' ') continue;
		length++;
	}

	char *newExpression = new char[length];
	i = 0;
	int index = 0;
	while (i < lengthExpression) {
		if (quadraticExpression[i] == ' ') {
			i++;
			continue;
		}
		newExpression[index++] = quadraticExpression[i++];
	}

	newExpression[index] = '\0';
	lengthExpression = length;
	delete[] quadraticExpression;
	quadraticExpression = newExpression;

}

void QuadraticExpression::analyzeString()
{
	deleteSpaces();

	int index = 0;
	while (quadraticExpression[index++] != 'x' && quadraticExpression[index++] != 'X');
	int length = index;

	char *leadingCoefficient = new char[length];
	int i = 0; 
	index = 0;
	while (quadraticExpression[i] != 'x' && quadraticExpression[i] != 'X')
		leadingCoefficient[index++] = quadraticExpression[i++];
	leadingCoefficient[index] = '\0';

	length = 0; i += 2; index = i;

	while (quadraticExpression[index++] != '+' && quadraticExpression[index++] != '-') length++;
	char *degree = new char[length];
	index = 0;
	while (quadraticExpression[i] != '+' && quadraticExpression[i] != '-')
		degree[index++] = quadraticExpression[i++];
	degree[index] = '\0';

	length = 0; i++; index = i;

	while (quadraticExpression[index++] != 'x' && quadraticExpression[index++] != 'X') length++;
	char *coefficientX = new char[length];
	index = 0;
	while (quadraticExpression[i] != 'x' && quadraticExpression[i] != 'X')
		coefficientX[index++] = quadraticExpression[i++];
	coefficientX[index] = '\0';

	length = 0; i += 2; index = i;
	
	while (index++ <= lengthExpression) length++;
	index = 0;
	char *freeCoefficient = new char[length];
	while (i < lengthExpression)
		freeCoefficient[index++] = quadraticExpression[i++];
	freeCoefficient[index] = '\0';

	this->leadingCoefficient = expressionToInt(leadingCoefficient);
	this->coefficientX = expressionToInt(coefficientX);
	this->degree = expressionToInt(degree);
	this->freeCoefficient = expressionToInt(freeCoefficient);

}

int QuadraticExpression::lengthStr(const char* const str)
{
	int i = 0;
	while (str[i++] != '\0');

	return (i - 1);
}

void QuadraticExpression::intToString(int number, char *str) {
	int length = 0;
	int copyNumber = number;
	while (copyNumber > 0) {
		copyNumber /= 10;
		length++;
	}
	char *s = new char[length + 1];
	s[length] = '\0';
	for (int i = length - 1; i >= 0; i++){
		s[i] = number % 10;
		number /= 10;
	}
	delete[] str;
	str = s;
}

int QuadraticExpression::expressionToInt(const char *str)
{
	int length = lengthStr(str);
	int *numbers = new int[length];
	bool result = stringToIntArray(str, numbers, length);

	int number = 0, order = 1;
	for (int i = 0; i < length - 1; i++)
		order *= 10;
	int i = 0;
	while (order > 0) {
		number += numbers[i++] * order;
		order /= 10;
	}

	delete[] numbers;

	if (!result) return 0;

	return number;
}

bool QuadraticExpression::stringToIntArray(const char *str, int *arr, const int size)
{
	bool result = true;
	for (int i = 0; i < size; i++) {
		if (str[i] <= '0' || str[i] >= '9') {
			result = false;
			break;
		}
		arr[i] = str[i] - '0';
	}

	return result;
}

QuadraticExpression::QuadraticExpression()
{
}

QuadraticExpression::QuadraticExpression(char *str)
{
	lengthExpression = lengthStr(str);
	quadraticExpression = new char[lengthExpression];
	for (int i = 0; i < lengthExpression; i++)
		quadraticExpression[i] = str[i];

	analyzeString();
}

QuadraticExpression::QuadraticExpression(std::string str)
{
	lengthExpression = str.length();
	quadraticExpression = new char[lengthExpression];
	for (unsigned int i = 0; i <= str.length(); i++) quadraticExpression[i] = str[i];

	analyzeString();
}

QuadraticExpression::QuadraticExpression(const QuadraticExpression &expression)
{
	lengthExpression = expression.lengthExpression;
	quadraticExpression = new char[lengthExpression];
	for (int i = 0; i < lengthExpression; i++) quadraticExpression[i] = expression.quadraticExpression[i];
	leadingCoefficient = expression.leadingCoefficient;
	coefficientX = expression.coefficientX;
	freeCoefficient = expression.freeCoefficient;
	degree = expression.degree;
}

std::ostream& operator<<(std::ostream& out, QuadraticExpression const expression) {
	for (int i = 0; i < expression.lengthExpression; i++) {
		out << expression.quadraticExpression[i];
	}

	return out;
}


QuadraticExpression::~QuadraticExpression()
{
	delete[] quadraticExpression;
}
