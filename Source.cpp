#include <iostream>
#include "QuadraticExpression.h"

using std::cout;
using std::cin;
using std::endl;

int main() {
	std::string input;
	getline(cin, input);
	QuadraticExpression quadratic(input);

	return 0;
}